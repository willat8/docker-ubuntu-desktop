FROM ubuntu:devel

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    tzdata \
    locales \
    xmonad \
    libghc-xmonad-extras-dev \
    # For startx
    xinit \
    # For xrdb
    x11-xserver-utils \
    # For Xorg.wrap
    xserver-xorg-legacy \
    xserver-xorg-input-libinput \
    brightnessctl \
    rxvt-unicode \
    sudo \
    vim \
    software-properties-common \
    gpg-agent \
    git \
    openssh-client \
    libavcodec-extra \
    pulseaudio \
    intel-media-va-driver \
    file \
    unzip \
    xz-utils \
    bzip2 \
    rename \
    apt-transport-https \
    gpgsm \
    libsasl2-modules \
    dnsutils \
 && rm -rf /var/lib/apt/lists/*

ADD https://packages.cloud.google.com/apt/doc/apt-key.gpg /usr/share/keyrings/kubernetes-archive-keyring.gpg
ADD https://baltocdn.com/helm/signing.asc /usr/share/keyrings/helm-signing.asc

RUN sed -ri 's@(deb http://archive.ubuntu.com/ubuntu/ )([^[:space:]-]+)( universe)@\1\2\3\n\1\2-proposed\3@' /etc/apt/sources.list \
 && chmod 644 /usr/share/keyrings/kubernetes-archive-keyring.gpg /usr/share/keyrings/helm-signing.asc \
 && echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list \
 && echo "deb [signed-by=/usr/share/keyrings/helm-signing.asc] https://baltocdn.com/helm/stable/debian/ all main" > /etc/apt/sources.list.d/helm-stable-debian.list \
 && add-apt-repository -y ppa:willat8/gphoto \
 && add-apt-repository -y ppa:willat8/minipro \
 && apt-get install -y --no-install-recommends \
    xmobar \
    firefox \
    gphoto2 \
    gphotofs \
    ffmpeg \
    minipro \
    kodi \
    kodi-pvr-mythtv \
    kubectl \
    helm \
    mutt \
 && rm -rf /var/lib/apt/lists/* # redo18

COPY group passwd shadow /etc/
COPY keyboard /etc/default

RUN sed -i 's/console/anybody/' /etc/X11/Xwrapper.config \
 && echo "needs_root_rights=yes" >> /etc/X11/Xwrapper.config \
 && echo "will ALL = (root) NOPASSWD: /lib/systemd/systemd-udevd -d, /usr/bin/udevadm trigger" | EDITOR="tee -a" visudo \
 && chmod 640 /etc/shadow

# Prevent urxvtd from spawning a second process
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=733323
RUN chgrp root /usr/bin/urxvtd \
 && chmod g-s /usr/bin/urxvtd

RUN chmod 4711 /usr/bin/brightnessctl

RUN ln -sfv /usr/share/zoneinfo/Australia/Sydney /etc/localtime \
 && locale-gen en_AU.UTF-8
USER will
# Substitute for invoking /bin/login
ENV SHELL=/bin/bash \
    PATH="${PATH}:/home/will/.local/bin" \
    LANG=en_AU.UTF-8 \
    HISTSIZE= HISTFILESIZE=
WORKDIR /home/will

ENTRYPOINT sudo /lib/systemd/systemd-udevd -d && sudo udevadm trigger && pulseaudio -D && exec startx

